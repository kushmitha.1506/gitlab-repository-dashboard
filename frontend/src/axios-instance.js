import axios from 'axios';

function directusAxios(context) {
  const instance = axios.create({
    baseURL: (process.env.NODE_ENV === 'production' && !window.Cypress) ? "directus/" : context.state.env.VUE_APP_DIRECTUS_URL
  });
  return instance;
}
export default directusAxios;