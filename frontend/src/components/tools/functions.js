export function showSuccessAlert() {
  this.$message({
    showClose: true,
    message: "Creation has been successfully sent to queue.",
    type: "success",
  });
}
export function showErrorAlert() {
  this.$message({
    showClose: true,
    message: "Creation was not successfully sent to queue.",
    type: "error",
  });
}
export function clearForm(context, model) {
  context['model'] = model
}

export function cancelForm(context, model) {
  context.visible = false;
  clearForm(context, model)
}