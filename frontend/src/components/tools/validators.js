export const includesRule = (rule, value, callback, subStr) => {
  if (value.includes(subStr))
    callback();
  else
    callback(new Error(`Field does not contain ${subStr}`));

}