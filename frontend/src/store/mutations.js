import Vue from "vue"


export default {
    SETENV(state, payload) {
        state.env = {}
        state.env.VUE_APP_DIRECTUS_URL = payload.VUE_APP_DIRECTUS_URL
        state.env.VUE_APP_DIRECTUS_USERNAME = payload.VUE_APP_DIRECTUS_USERNAME
        state.env.VUE_APP_DIRECTUS_PASSWORD = payload.VUE_APP_DIRECTUS_PASSWORD
        state.env.VUE_APP_GOOGLE_CLIENT_ID = payload.VUE_APP_GOOGLE_CLIENT_ID
        state.env.VUE_APP_DIRECTUS_TOKEN = payload.VUE_APP_DIRECTUS_TOKEN
        state.env.VUE_APP_DASHBOARD_ADMIN = payload.VUE_APP_DASHBOARD_ADMIN
        state.env.VUE_APP_CWV_APPROVER_ADMIN = payload.VUE_APP_CWV_APPROVER_ADMIN
    },
    
    SET_TABLE_FIELD(state, payload) {
        const { parentField, childField, data } = payload
        Vue.set(state[parentField], childField, data)
    },
    SETTOTAL(state, payload) {
        state.total = payload;
    },
    UPDATE_PAGE(state, payload) {
        state.query.page = payload;
    },
    UPDATE_LIMIT(state, payload) {
        state.query.limit = payload;
    },
    SET_LOADING(state, payload) {
        state.loading = payload
    },
    SETTABLE(state, payload) {
        state.tableData = payload;
    },
    UPDATE_TABLE_DATA(state, payload) {
        const data = state.tableData;
        data.unshift(payload);
        state.tableData = data
        state.total = state.total + 1
    },
    UPDATE_SEARCH_INPUT(state, payload) {
        state.query.searchInput = payload
    },
    UPDATE_PIPELINE_TABLE(state,payload) {
        state.pipelineTable = payload
    }

}