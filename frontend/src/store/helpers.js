export function getHeaders(context) {
  const res = {
    'Content-Type': 'application/json',
  }
  if (process.env.NODE_ENV !== 'production' || window.Cypress) res['Authorization'] = 'Bearer ' + context.state.env.VUE_APP_DIRECTUS_TOKEN
  return res
}
export const calculateAPI = (filters) => {
  let api = {};
  api = {
    'filter': {
    }
  }
  for (const filter of filters) {
    const { field, input, rule } = filter
    api['filter'][field] = {}
    api['filter'][field][rule] = input
  }
  return api;
}

export const calculateFieldStr = (fields) => {
  if (!fields || fields.length == 0) return ""
  else {
    let fieldStr = "fields="
    for (let field of fields) {
      fieldStr = fieldStr + field + ","
    }
    fieldStr = fieldStr.slice(0, -1)
    return fieldStr;
  }
}

export function access(object, field) {
  let properties = field.split(".")
  for (let property of properties) {
    // go to deeper into object until your reached final
    object = object[property];
  }
  return object;
}
