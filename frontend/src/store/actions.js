import axios from 'axios'
import directusAxios from '../axios-instance';
import * as helpers from "./helpers";
export default {
  async loginDirectus(context, payload) {
    const { email, first_name, last_name, token } = payload;
    const getParams = {
      url: "/users",
      filters: [
        {
          field: "email",
          input: email,
          rule: "_eq",
        },
      ],
    }
    let result = await context.dispatch("getField", getParams)
    if (result && result[0]) return result[0].id
    else {
      await context.dispatch("createField", {
        url: "/custom/register",
        data: {
          email,first_name, last_name, token
        }
      })
      return (await context.dispatch("getField", getParams))[0].id
    }
  },
  async getENV(context) {
    let res = {};
    res.VUE_APP_DASHBOARD_ADMIN = process.env.VUE_APP_DASHBOARD_ADMIN
    res.VUE_APP_CWV_APPROVER_ADMIN = process.env.VUE_APP_CWV_APPROVER_ADMIN
    if (process.env.NODE_ENV === 'production' && !process.env.VUE_APP_DIRECTUS_URL) {
      res = await axios.get("/env")
      res = res.data
    }
    else {
      res.VUE_APP_DIRECTUS_URL = process.env.VUE_APP_DIRECTUS_URL
      res.VUE_APP_DIRECTUS_USERNAME = process.env.VUE_APP_DIRECTUS_USERNAME
      res.VUE_APP_DIRECTUS_PASSWORD = process.env.VUE_APP_DIRECTUS_PASSWORD
      res.VUE_APP_GOOGLE_CLIENT_ID = process.env.VUE_APP_GOOGLE_CLIENT_ID
      res.VUE_APP_DIRECTUS_TOKEN = process.env.VUE_APP_DIRECTUS_TOKEN
    }
    context.commit('SETENV', res)
  },
  getField(context, payload) {
    let api = undefined;
    const { filters, url, fields, location, getCallbacks } = payload;
    const fieldStr = helpers.calculateFieldStr(fields)
    if (payload.filters) api = helpers.calculateAPI(filters)
    return new Promise((resolve, reject) => {
      directusAxios(context).get(url + (fieldStr != "" ? "?" + fieldStr : ""), { params: api, headers: helpers.getHeaders(context) }).then(async (res) => {
        let data = res.data.data.reverse()
        if (getCallbacks) {
          for (const callback of getCallbacks)
            data = await callback(data)
        }
        if (location) {
          const { parentField, childField } = location
          context.commit('SET_TABLE_FIELD', { parentField, childField, data })
        }
        resolve(data);
      }).catch(e =>
        reject(e.response.data) //TODO
      );
    });
  },
  alterTablePart(context, payload) {
    if (window.Cypress) return { status: 200 };
    const { request, data, url, fields, location, postCallbacks, getCallbacks, action } = payload
    let urlId = request == 'post' ? "" : ("/" + data.repo_id)
    // let removedFields = []

    const alteredData = JSON.parse(JSON.stringify(data))
    // removing fields because can mess with initial post. 
    // EX: if trying to post an id via a callback but kept in initial post, the initial post will fail.
    if (postCallbacks) {
      for (const callback of postCallbacks) {
        const params = callback.params
        if (params && params.dataField) {
          delete alteredData[params.dataField]
        }
      }

    }
    // calculates fields. ex: "id, name, url..."
    const fieldStr = helpers.calculateFieldStr(fields)
    // console.log("firedStr", fieldStr);
    delete alteredData.id
    return new Promise((resolve) => {
      directusAxios(context)[request](url + urlId + `?${fieldStr}`, data, { 'headers': helpers.getHeaders(context) })
        .then(async (res) => {
          if (postCallbacks) {
            for (const callback of postCallbacks) {
              // readds data postCallbacks need then calls the callback
              const { action, params } = callback;
              const payload = {
                data: data,
                resData: res.data.data,
                params: params
              }
              await context.dispatch(action, payload)
            }
            res = await directusAxios(context).get(url + `/${res.data.data.id}?${fieldStr}`, { 'headers': helpers.getHeaders(context) })
          }
          let resData = res.data.data
          if (getCallbacks) {
            for (const callback of getCallbacks) {
              resData = (await callback([resData]))[0]
            }
          }
          if (!data.id && urlId != "") data.id = urlId
          // does something different with results depending on what you're trying to do
          // createTableData: creating data for a table to use
          // createField: adding a new option to a particular field, then either updating the data or just resolving
          // see index.js and mutations.js for implementation 
          if (action === "createTableData") {
            if (request == 'post') context.commit('UPDATE_TABLE_DATA', resData)
            else {
              context.commit("SET_LOADING", true)
              await context.dispatch("getSearchResults", payload)
              context.commit("SET_LOADING", false)
            }

          }
          if (location) {
            const { parentField, childField } = location
            if (request == 'post') context.commit('UPDATE_FORM_FIELD', { data: resData, parentField, childField })
            else context.dispatch("getField", payload)
          }
          resolve(res)
        }).catch(e => {
          console.error()
          //console.log(e)
          resolve(e.response)
        })
    })
  },
  editTableData(context, payload) {
    payload["action"] = "createTableData"
    payload["request"] = "patch"
    return context.dispatch("alterTablePart", payload)
  },
  createField(context, payload) {
    payload["action"] = "createField";
    payload["request"] = "post";
    return context.dispatch("alterTablePart", payload);
  },
  async setUpPrefixes(context) {
    let data = await context.dispatch("getField", {
      url: "/items/sys_platform_tools",
    });
    for (const app of data) {
      switch (app.title) {
        case "Design Book":
          context.commit('SET_TABLE_FIELD',
            {
              parentField: 'design',
              childField: 'prefix',
              data: app.prefix
            })
          break
        case "Repository":
          context.commit('SET_TABLE_FIELD',
            {
              parentField: 'repository',
              childField: 'prefix',
              data: app.prefix
            })
          break
      }
    }
  },
  async setupRepoFields(context, payload) {
    return context.dispatch("getField", payload)
  },
  async createManyToManyConnection(context, payload) {
    const { data, resData, params } = payload
    const { parentIdName, childIdName, url, dataField } = params
    let payload2 = undefined
    if (data[dataField])
      for (const id of data[dataField]) {
        payload2 = {}
        payload2[parentIdName] = resData.id
        payload2[childIdName] = id
        await directusAxios(context).post(url, payload2, { 'headers': helpers.getHeaders(context) })

      }

  },
  async deleteManyToManyConnection(context, payload) {
    const { data, resData, params } = payload
    const { parentIdName, url, dataField } = params
    if (data[dataField]) {
      const delObjects = await context.dispatch("getField", {
        url: url,
        filters: [{
          field: parentIdName,
          input: resData.id,
          rule: "_eq",
        }]
      });
      for (const obj of delObjects) //in - index//of-objects
        await directusAxios(context).delete(url + '/' + obj.id, { 'headers': helpers.getHeaders(context) })
    }
  },
  async updateOrCreate(context, payload) {
    const { getParams, editParams, postParams } = payload
    //getting
    const childData = (
      await context.dispatch("getField", getParams)
    )[0];
    let res = null;
    if (childData) { // editing
      editParams["data"]["id"] = childData.id
      res = await context.dispatch("editField", editParams);
      delete editParams["data"]["id"]
    }
    else   // posting
      res = await context.dispatch("createField", postParams);

    return (res.data.data)
  },
  editField(context, payload) {
    payload["action"] = "editField"
    payload["request"] = "patch"
    return context.dispatch("alterTablePart", payload)
  },
  getSearchResults(context, payload) {
    const { getCallbacks, fields, searchField, url } = payload
    const fieldStr = helpers.calculateFieldStr(fields)
    const api = context.state.query.searchInput ?
      helpers.calculateAPI([{
        field: searchField ? searchField : 'name',
        input: context.state.query.searchInput,
        rule: '_contains'
      }]) : undefined;
    return new Promise((resolve, reject) => {
      directusAxios(context).get(`${url}?meta=filter_count${(fieldStr != "" ? "&" + fieldStr : "")}&limit=${context.state.query.limit}&page=${context.state.query.page}`, { params: api, headers: helpers.getHeaders(context) }).then(async (res) => {
        let data = res.data.data.reverse()
        if (getCallbacks) {
          for (const callback of getCallbacks)
            data = await callback(data)
        }
        context.commit('SETTABLE', data);
        let total = res.data.meta.filter_count
        if (res.data.data.length == 0) total = 0;
        context.commit('SETTOTAL', total)
        resolve(res.data);
      }).catch(e => {
        console.error()
        // console.log(e)
        reject(e)
        //reject(e.response.data)
      }
      );
    });
  },
  createTableData(context, payload) {
    // console.log("in createTableData", payload);
    payload["action"] = "createTableData"
    payload["request"] = "post"
    return context.dispatch("alterTablePart", payload)
  },
  setWebHook(context, payload) {
    let repo_id = payload.repo_id
    let fullurl = new URL(payload.repo_url);
    let baseurl = fullurl.origin;
    const config = {
      method: "post",
      url: '/setWebHook',
      data: {
        requrl: baseurl,
        repo_id: repo_id,
      },
    }
    axios(config)
      .then(() => {
        console.log("WebHook Added")
      })
      .catch((err) => {
        console.log("webhook error",err);
      });
  },
  protectBranch(context, payload) {
    let repo_id = payload.repo_id
    let fullurl = new URL(payload.repo_url);
    let baseurl = fullurl.origin;
    const config = {
      method: "post",
      url: '/protect-branches',
      data: {
        requrl: baseurl,
        repo_id: repo_id,
      },
    }
    axios(config)
      .then(() => {
        console.log("Branches protected")
      })
      .catch((err) => {
        console.log("protect error",err);
      });
    // axios.post
  },
  addDevloper(context, payload){
    let baseurl = payload.gitlab_url;
    let project_id = payload.project_id;
    let developer = payload.devtobeadded;

    const config = {
      method: "post",
      url: '/add-devloper',
      data: {
        requrl: baseurl,
        project_id: project_id,
        developer : developer
      },
    }
    axios(config)
      .then((res) => {
        console.log(res.data)
        return res.data
      })
      .catch((err) => {
        console.log("add-devloper :",err);
      });
  },
  async createProject(context, payload) {
    let baseurl = payload.gitlab_url;
    let repo_name = payload.repo_name
    let resData = undefined
    const config = {
      method: "post",
      url: '/create-project',
      data: {
        requrl: baseurl,
        repo_name : repo_name
      },
    }
    await axios(config)
      .then((res) => {
        // console.log(res.data)
        resData = res.data
      })
      .catch((err) => {
        console.log("create project :",err);
      });
    return resData
  },
  async searchRepo(context, payload) {
    let baseurl = payload.gitlab_url;
    // console.log(baseurl)
    let encodedpath = payload.encodedpath
    let resData = undefined
    const config = {
      method: "post",
      url: '/search-repo-data',
      data: {
        requrl: baseurl,
        encodedpath: encodedpath,
      },
    }
    await axios(config)
      .then((res) => {
        // console.log(res.data)
        resData = res.data
      })
      .catch((err) => {
        console.log("create project :",err);
      });
    return resData
  },
  async retryjob(context, payload) {
    let resData = undefined
    const config = {
      method: "post",
      url: '/retry-job',
      data: payload,
    }
    await axios(config)
      .then((res) => {
        // console.log(res.data)
        resData = res.data
      })
      .catch((err) => {
        console.log("create project :",err);
      });
    return resData
  },
  async manualjob(context, payload) {
    let resData = undefined
    const config = {
      method: "post",
      url: '/manual-job',
      data: payload,
    }
    await axios(config)
      .then((res) => {
        // console.log(res.data)
        resData = res.data
      })
      .catch((err) => {
        console.log("create project :",err);
      });
    return resData
  },
  async canceljob(context, payload) {
    let resData = undefined
    const config = {
      method: "post",
      url: '/cancel-job',
      data: payload,
    }
    await axios(config)
      .then((res) => {
        // console.log(res.data)
        resData = res.data
      })
      .catch((err) => {
        console.log("create project :",err);
      });
    return resData
  },
  async triggerpipeline(context, payload) {
    let resData = undefined
    const config = {
      method: "post",
      url: '/triggernew-pipeline',
      data: payload,
    }
    await axios(config)
      .then((res) => {
        // console.log(res.data)
        resData = res.data
      })
      .catch((err) => {
        console.log("create project :",err);
      });
    return resData
  },
  async approvepipeline(context, payload) {
    let resData = undefined
    const config = {
      method: "post",
      url: '/approve-pipeline',
      data: payload,
    }
    await axios(config)
      .then((res) => {
        resData = res.data
      })
      .catch((err) => {
        console.log("approve staging pipeline :",err);
      });
    return resData
  },
  async getPipelines(context, payload) {
    let resData = []
    const config = {
      method: "post",
      url: '/getpipelinebyid',
      data: payload,
    }
    await axios(config)
      .then((res) => {
        //console.log(res.data)
        resData.push(res.data)
      })
      .catch((err) => {
        console.log("error :",err);
      });
    return resData
  },
}
