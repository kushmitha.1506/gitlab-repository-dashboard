import Vue from 'vue';
import Vuex from 'vuex';

import localStorageHandler from '../localStorageHandler';
import actions from "./actions";
import mutations from "./mutations";

Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        env: {},
        tableData : [],
        applnData: [],
        inputurl : undefined,
        total: 0,
        email: localStorageHandler.getItem('email') || 'aayushpatniya1999@gmail.com',
        userId: localStorageHandler.getItem('userId') || 'aayushpatniya1999@gmail.com',
        userName : localStorageHandler.getItem('userName') || 'aayushpatniya1999',
        loading: false,
        query: {
            searchInput: '',
            page: 1,
            limit: 20,
        },
        repository: {
            emails : [],
            approvers : [],
            // prefix : ""
        },
        pipelineTable: []
    },
    mutations,
    actions, 
})