// router.js
import Vue from 'vue';
import Router from 'vue-router';


Vue.use(Router);

export default new Router({
	routes: [
		{
			path: '/home',
			name: 'home',
			component: () => import('./views/Home.vue')
		},
		{
			path: '/merge-screen/:project_id/:merge_request_id/merge',
			name: 'merge-screen',
			component: () => import('./views/ScreenMerge.vue')
		},
		{
			path: '/merge-screen/:project_id',
			name: 'MergeRequestList',
			component: () => import('./views/MergeRequestList.vue')
		},
		{
			path: '/',
			name: 'ApproveScreen',
			component: () => import('./views/ApproveScreen.vue')
		},
	],
	mode:'history'
});

