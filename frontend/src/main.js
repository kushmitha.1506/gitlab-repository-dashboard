import Vue from 'vue';
import ElementUI from 'element-ui';

import App from './App.vue';
import store from './store';
import router from './router';

import Axios from 'axios';
Vue.prototype.$axios = Axios

import GAuth from 'vue-google-oauth2'

// using element ui 
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en';
Vue.use(ElementUI, { locale });

new Vue({
  store,
  router,
  el: '#app',
  render: h => h(App)
});
if (window.Cypress) {
  window.store = store
}
// setting up the gauth
async function setup() {
  await store.dispatch("getENV");
  const gAuthOptions = {
    clientId: store.state.env.VUE_APP_GOOGLE_CLIENT_ID,
    scope: "email",
    prompt: "consent",
    fetch_basic_profile: true,
  };
  Vue.use(GAuth, gAuthOptions)

}
setup();