const path = require("path");
const isProd = false;

module.exports = {
  publicPath: isProd ? `/static_vue/gitlab-repository-dashboard/` : '/', 
  outputDir: path.resolve(__dirname, "../backend/public/gitlab-repository-dashboard"),
  devServer: {
    proxy: 'http://localhost:8080',
  },
};