const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ extended: false }));

const path = require("path");

const cors = require('cors');
app.use(cors());

const axios = require('axios');
const e = require('express');

require('dotenv').config()
const port = parseInt(process.env.PORT, 10) || 8080;
app.set('port', port);


// using vue build from public 
app.use('/static_vue', express.static(path.resolve(__dirname, './public')));

// sending vue build htmls and packagaes
app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, './public/gitlab-repository-dashboard/index.html'));
});

// to check live status of api in deployment
app.get('/health', (req, res) => res.status(200).send({
    message: 'Get Call I am alive',
}));



app.get('/:project_id/:merge_request_id/merge', function (request, response) {

    const fullUrl = `https://gitlab.com/api/v4/projects/${request.params.project_id}/merge_requests/${request.params.merge_request_id}/merge`

    const config = {
        method: "put",
        url: fullUrl,
        headers: { 'PRIVATE-TOKEN': 'glpat-8h67cjkMEq7HynAtrEdz' }
    }
    axios(config)
        .then((res) => {
            return response.status(200).send(res.data);
        }).catch((error) => {
            console.log(error)
            return response.status(400).send(error);
        })
});

app.get('/:project_id/:merge_request_id/approvals', function (request, response) {

    const fullUrl = `https://gitlab.com/api/v4/projects/${request.params.project_id}/merge_requests/${request.params.merge_request_id}/approvals`

    const config = {
        method: "get",
        url: fullUrl,
        headers: { 'PRIVATE-TOKEN': 'glpat-8h67cjkMEq7HynAtrEdz' }
    }
    axios(config)
        .then((res) => {
            return response.status(200).send(res.data.approved_by);
        }).catch((error) => {
            console.log(error)
            return response.status(400).send(error);
        })
});

// all merge request api 
app.get('/:project_id/merge_requests', function (request, response) {
    let url = `https://gitlab.com/api/v4/projects/${request.params.project_id}/merge_requests`
    const config = {
        method: "get",
        url: url,
        headers: { 'PRIVATE-TOKEN': 'glpat-8h67cjkMEq7HynAtrEdz' }
    }
    axios(config)
        .then((res) => {
            let resp = []
            let data = {}
            res.data.forEach(s => {
                data = {
                "state" : s.state,
                "MR_id" : s.iid,
                "MR_url" : s.web_url,
                "author" : s.author.name,
                "has_conflicts":s.has_conflicts
                }
                resp.push(data)
              });
            // console.log(resp);
            return response.status(200).send(resp);
        }).catch((error) => {
            console.log(error)
            return response.status(400).send(error);
        })
})

// creating server on specified port
app.listen(port, () => {
    console.log("server running on port :" + port)
})