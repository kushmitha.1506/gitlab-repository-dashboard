const gulp = require('gulp');
const exec = require('child_process').exec;

gulp.task('fe-install', function (cb) {
    return exec('cd frontend && npm i --no-optional --silent', function (err, stdout, stderr) {
        cb(err);
    });
});

gulp.task('be-install', function (cb) {
    return exec('cd backend && npm i --no-optional', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('fe-build', function (cb) {
    return exec('cd frontend && npx vue-cli-service build', function (err, stdout, stderr) {
        cb(err);
    });
});

gulp.task('copy', function () {
    return gulp.src('./backend/**/*')
        .pipe(gulp.dest('./dist'));
});


gulp.task('default', gulp.series(
    gulp.parallel('fe-install', 'be-install'),
    'fe-build',
    'copy',
    function (done) {
        console.log('done');
        done();
    })
);
