# Gitlab Repository Dashboard

This Website is a tool that works with gitlab free version and gives you a feature to have approvers, Merge repository and directly create project from the dashbaord

## Requirements

Clone the develop branch and work from there.


You need to place a .env file in the frontend and backend for it to run locally.


Env file should look like this: 

```
VUE_APP_DIRECTUS_URL = "https://directus-engineeringdata.k8s.devint.webmd.com"
VUE_APP_GOOGLE_CLIENT_ID = "398765350264-lqjsj1hs0elqmkom14vnsgkq57vbnq75.apps.googleusercontent.com"
VUE_APP_DIRECTUS_TOKEN = "tokenToAccessWith"
VUE_APP_REPOSITORY_URL = "https://gitlab-repository-dashboard.k8s.devint.webmd.com"
GIT_INTERNETBRAND_TOKEN = "TOKEN HERE"
MA1_GIT_INTERNETBRAND_TOKEN = "TOKEN HERE"
PROTECTED_BRANCHES = "develop, *release*"
GITLAB_WEBMD_TOKEN = "TOKEN HERE"
VUE_APP_DASHBOARD_ADMIN = "mshaikh@webmd.net,kradhakrishnan@webmd.net,svenkatesan@webmd.net"

NOTE : FOR TOKEN CONNECT WITH YOUR MANAGERS
```


To run the application locally, go to the frontend and run "npm i" then "npm run serve".
Then go to the backend and run "npm i" then "npm start".


Open the localhost with the port number specified eg: http://localhost:8080/#/



Once deployed to devint, make sure to have https:// in the url or login won't work.


## How to Use

This application uses a lot of reusable code found in functionality in the store folder.



Read through these functions, specifically in actions.js, to see how they work.



Their use cases are found in the views and components.

### Some Noteworthy Mentions:



AlterTablePart (actions.js)

- Function that posts or edits depending on the information you give it.
- Other functions in actions.js passes information.
- Proceeds to store into a variable in actions.js, either tableData if a part of the table being displayed and/or a specific field if specified a location.

ApiParams (views)

- url: The url which to call directus with to get/post the data.
- searchField: Which parameter to search for in getSearchResults (subject to change)
- fields: The fields that should be returned when getting.
- getCallbacks: Functions to call to manipulate the data after it is gotten.
- postCallbacks: Functions to call that manipulate other tables after posting to the base url. See createManyToManyConnection for a callback example.
- data: This gets added to apiParams once a form is complete, the data you want to post.

State (index.js)

- Currently, variables being used as a part of an application are stored inside the application. This is typically used in posting. Ex: Categories in curriculum.

Logging 

Create a created lifecycle and you

## Api calls:

All the api calls should send from backend Hence frontend calls store functions which creates an axios request for node.js backend then backend does all the api calls and send back the data to action.js file.

for eg.
-setwebhook
